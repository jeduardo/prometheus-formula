describe user('prometheus') do
  it { should exist }
end

describe group('prometheus') do
  it { should exist }
end

describe file('/etc/prometheus') do
 its('type') { should eq :directory }
 it { should be_directory }
 its('owner') { should eq 'root' }
 its('group') { should eq 'root' }
 its('mode') { should cmp '0755' }
end

describe file('/etc/prometheus/prometheus.yml') do
 its('type') { should eq :file }
 it { should be_file }
 its('owner') { should eq 'root' }
 its('group') { should eq 'root' }
 its('mode') { should cmp '0644' }
end

describe file('/etc/prometheus/rules') do
 its('type') { should eq :directory }
 it { should be_directory }
 its('owner') { should eq 'root' }
 its('group') { should eq 'root' }
 its('mode') { should cmp '0755' }
end

describe file('/etc/prometheus/tgroups') do
 its('type') { should eq :directory }
 it { should be_directory }
 its('owner') { should eq 'root' }
 its('group') { should eq 'root' }
 its('mode') { should cmp '0755' }
end

describe file('/var/lib/prometheus') do
 its('type') { should eq :directory }
 it { should be_directory }
 its('owner') { should eq 'prometheus' }
 its('group') { should eq 'prometheus' }
 its('mode') { should cmp '0755' }
end

describe file('/etc/systemd/system/prometheus.service') do
  its('type') { should eq :file }
  it { should be_file }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0644' }
  its('content') { should match /--storage.tsdb.retention.time=/}
  its('content') { should match /--log.level=debug/}
end

describe service('prometheus') do
  before(:all) do
        sleep(5)
  end
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
