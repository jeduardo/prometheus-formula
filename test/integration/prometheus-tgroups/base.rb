describe user('prometheus') do
  it { should exist }
end

describe group('prometheus') do
  it { should exist }
end

describe file('/etc/prometheus') do
 its('type') { should eq :directory }
 it { should be_directory }
 its('owner') { should eq 'root' }
 its('group') { should eq 'root' }
 its('mode') { should cmp '0755' }
end

describe file('/etc/prometheus/prometheus.yml') do
 its('type') { should eq :file }
 it { should be_file }
 its('owner') { should eq 'root' }
 its('group') { should eq 'root' }
 its('mode') { should cmp '0644' }
end

describe file('/etc/prometheus/tgroups') do
 its('type') { should eq :directory }
 it { should be_directory }
 its('owner') { should eq 'root' }
 its('group') { should eq 'root' }
 its('mode') { should cmp '0755' }
end

describe file('/etc/prometheus/tgroups/app_servers.yml') do
 its('type') { should eq :file }
 it { should be_file }
 its('owner') { should eq 'root' }
 its('group') { should eq 'root' }
 its('mode') { should cmp '0644' }
end

describe file('/var/lib/prometheus') do
 its('type') { should eq :directory }
 it { should be_directory }
 its('owner') { should eq 'prometheus' }
 its('group') { should eq 'prometheus' }
 its('mode') { should cmp '0755' }
end

describe file('/etc/systemd/system/prometheus.service') do
  its('type') { should eq :file }
  it { should be_file }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0644' }
  its('content') { should match /--storage.tsdb.retention.time=/}
  its('content') { should match /--log.level=info/}
end

describe service('prometheus') do
  before(:all) do
        sleep(10)
  end
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

# This needs to be after the `describe service('prometheus') do` block because that sleeps log enough to make sure the metrics are
# there in this test. This isn't
describe bash('/opt/prometheus/dist/prometheus-2.9.1/promtool query series --match=up http://localhost:9090') do
  its('stdout') { should match /{__name__="up", instance="host1:8080", job="test", label1="labelvalue1", label2="labelvalue2"}/ }
  its('exit_status') { should eq 0 }
end
