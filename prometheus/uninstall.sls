# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "prometheus/map.jinja" import prometheus with context %}

prometheus-remove-service:
  service.dead:
    - name: prometheus
  file.absent:
    - name: /etc/systemd/system/prometheus.service
    - require:
      - service: prometheus-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: prometheus-remove-service

prometheus-remove-symlink:
   file.absent:
    - name: {{ prometheus.bin_dir}}/prometheus
    - require:
      - prometheus-remove-service

prometheus-remove-binary:
   file.absent:
    - name: {{ prometheus.dist_dir}}
    - require:
      - prometheus-remove-symlink

prometheus-remove-config:
    file.absent:
      - name: {{ prometheus.config_dir }}
      - require:
        - prometheus-remove-binary
