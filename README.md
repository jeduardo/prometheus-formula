#prometheus-formula

A saltstack formula created to setup [Prometheus monitoring server](https://www.prometheus.io).


# Available states

## init

The essential Prometheus state running both ``install`` and ``config``.

## install


- Download Prometheus release from Github into the dist dir  (defaults to /opt/prometheus/dist)
- link the binary (defaults to /usr/bin/prometheus)
- Register the appropriate service definition with systemd.

This state can be called independently.


## config

Deploy the configuration files required to run the service, and enable the
service if configured to do so.

Configuration is done through the ``prometheus`` pillar.

This state can be called independently.

## tgoups

Get target groups from the pillar and put them into yaml files in ``{{ prometheus.config_dir }}/tgroups``.
Those target groups can be used in prometheus jobs as source for scraping Machines.
Example prometheus job config
```
- job_name: "mongo"
  file_sd_configs:
  - files:
    - '/etc/prometheus/tgroups/mongo.yml
```

## uninstall

Remove the service, binaries, and configuration files. The data itself will be kept and needs to be removed manually, just to be on the safe side.

This is state must always be called independently.

# Testing

## Prerequisites
The tests are using test kitchen with [inspec](https://www.inspec.io/docs/reference/resources/) as verifier, so you need to have
- Ruby installed
- Python installed
- Docker installed

If you want to add tests you might want to take a look at the [documentation](http://testinfra.readthedocs.io/en/latest/modules.html#) for the modules.

## Running the test
If you run the tests for the first time you might need to run ``bundle install`` and ``pip install -r requirements.txt`` first. Afterwards you can run ``kitchen test --concurrency 12 --parallel``.  
Be aware that ``--concurrency 12 --parallel`` might be a bit too demanding for some machines. You can either ommit these parameters or adjust the to fit your machines capacity.
